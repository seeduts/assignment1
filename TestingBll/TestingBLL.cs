﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment1;
using System.Data.SqlClient;

namespace TestingBll
{
    [TestClass]
    public class TestingBLL
    {
        Bll B;
        SqlDataReader da = null;
        [TestInitialize]
        public void Setup()
       {
            B = new Bll();
        }
        [TestMethod]
        public void Test_listAllStaff()
        {
            Assert.AreSame (da,B.listAllStaff("a", "b"));
        }

        [TestMethod]
        public void Test_createClient()
        {
            Assert.AreEqual(1, B.createClient("A", "B"));
        }
        [TestMethod]
        public void Test_changePassword()
        {
            Assert.AreEqual(1, B.changePassword("A", "B"));
        }
        [TestMethod]
        public void Test_Login()
        {
            Assert.AreEqual(true, B.Login("A", "B"));
        }
        [TestMethod]
        public void Test_viewClientsDetail()
        {
            Assert.AreEqual(true, B.viewClientsDetail("A"));
        }
        [TestMethod]
        public void Test_CreateNewIntervention()
        {
            Assert.AreEqual(1, B.CreateNewIntervention());
        }
        [TestMethod]
        public void Test_listAllIntervention()
        {
            Assert.AreSame(da, B.listAllIntervention("A"));
        }
        [TestMethod]
        public void Test_changeStateByEngineer()
        {
            Assert.AreEqual(1, B.changeStateByEngineer(2, "Approved"));
        }
        [TestMethod]
        public void Test_listAllAproved()
        {
            Assert.AreSame(da, B.listAllAproved("a0"));
        }
        [TestMethod]
        public void Test_ManagerApprove()
        {
            Assert.AreEqual(1, B.ManagerApprove(1));
        }
        [TestMethod]
        public void Test_ChangeDistrict()
        {
            Assert.AreEqual(1, B.ChangeDistrict("a", "b"));
        }
        [TestMethod]
        public void runReport()
        {
            Assert.AreEqual(true, B.runReport());
        }
    }
}
