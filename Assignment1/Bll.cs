﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
   public  interface BusinessInterface
    {
       SqlDataReader listAllStaff();
        int createClient(string name,string district);
        int changePassword(string userName,string password);
        bool Login(string userName,string password);
        bool viewClientsDetail(string clientName);
        int CreateNewIntervention();
        SqlDataReader listAllIntervention(string userName);
        int changeStateByEngineer(int id,string state);
        SqlDataReader listAllAproved(string userName);
        int ManagerApprove(int id);
        int  ChangeDistrict(string district,string userName);
        bool runReport();

    }
    public class Bll:BusinessInterface
    {
        string connectionString = "Data Source=(local);Initial Catalog=Assignment1;Integrated Security=yes";
        
         public SqlDataReader listAllStaff()
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = "Select * from Staff";
                SqlCommand command = new SqlCommand(queryString, conn);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                return reader;
            }
        }
        
       public  int createClient(string clientName,string clientDistrict)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string insert = "Insert into Client (ClientName,ClientDistrict) values ('@name','@district')";
                SqlCommand command = new SqlCommand(insert, conn);
                command.Parameters.AddWithValue("@name", clientName);
                command.Parameters.AddWithValue("@district", clientDistrict);
                conn.Open();
                int i= command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }

        public int changePassword(string userName,string password)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string update = "Update user set password=@password where userName=@userName";
                SqlCommand command = new SqlCommand(update, conn);
                command.Parameters.AddWithValue("@password", password);
                command.Parameters.AddWithValue("@userName", userName);
                conn.Open();
                int i = command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }

        public bool Login(string userName, string password)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = "Select * from user where userName=@userName and password=@password";
                SqlCommand command = new SqlCommand(queryString, conn);
                command.Parameters.AddWithValue("@password", password);
                command.Parameters.AddWithValue("@userName", userName);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read()) return true;
                else return false;
            }
        }

       public  bool viewClientsDetail(string clientName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = "Select * from client inner join intervention on client.id=intervention.Client_ID where client.Name=@clientName";
                SqlCommand command = new SqlCommand(queryString, conn);
                command.Parameters.AddWithValue("@clientName", clientName);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read()) return true;
                else return false;
            }
        }

        public int CreateNewIntervention()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string insert = "Insert into Intervention values *******";
                SqlCommand command = new SqlCommand(insert, conn);
                conn.Open();
                int i = command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }

        public SqlDataReader listAllIntervention(string userName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = "Select * from intervention where userName=@userName";
                SqlCommand command = new SqlCommand(queryString, conn);
                command.Parameters.AddWithValue("@Name", userName);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                return reader;
            }
        }

       public  int changeStateByEngineer(int id,string state)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string update = "Update Intervention set state=@state where id=@id";
                SqlCommand command = new SqlCommand(update, conn);
                command.Parameters.AddWithValue("@state",state);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                int i = command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }
        public SqlDataReader listAllAproved(string userName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = "Select * from intervention where userName=@userName and state=approved";
                SqlCommand command = new SqlCommand(queryString, conn);
                command.Parameters.AddWithValue("@Name", userName);
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                return reader;
            }
        }
        public int ManagerApprove(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string update = "Update Intervention set state=@state where id=@id";
                SqlCommand command = new SqlCommand(update, conn);
                command.Parameters.AddWithValue("@id", id);
                conn.Open();
                int i = command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }

        public int ChangeDistrict(string district, string userName)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string update = "Update user set district=@district where name=@userName";
                SqlCommand command = new SqlCommand(update, conn);
                command.Parameters.AddWithValue("@district", district);
                command.Parameters.AddWithValue("@userName", userName);
                conn.Open();
                int i = command.ExecuteNonQuery();
                conn.Close();
                return i;
            }
        }
       public  bool runReport()
        {
            return true;
        }
    }
}
